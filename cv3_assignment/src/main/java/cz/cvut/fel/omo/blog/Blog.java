/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.blog;

import java.util.ArrayList;


/**
 *
 * @author jarda
 */
public class Blog {
 public ArrayList<Post> fullArticleList=new ArrayList<>();
 public ArrayList<Topic>fullTopicsList=new ArrayList<>();
 public ArrayList<User>Users=new ArrayList<>();
 public void createNewAccount(String name,String psw, boolean admin){
 if(admin){
     createAdmin(name, psw);
 }else{
     createLayman(name, psw);
 }
 
 }
 public void createLayman(String name, String psw){
     this.Users.add(new UserAccount(name, psw, this)); 
 
 }
 public void createAdmin(String name, String psw){
     this.Users.add(new AdminAccount(name, psw, this));
     
 
 }
 
 public User login(String name, String psw){
    return this.Users.stream()
             .filter(user ->name.equals(user.username))
             .filter(user->psw.equals(user.psw))
             .filter(user->user.getBlocked()!=true)
             .findFirst()
             .orElse(null);
            
     
  }
 public Topic findTopic(String topicName){
    return this.fullTopicsList.stream()
         .filter(topic -> topicName.equals(topic.name))
         .findFirst()
         .orElse(null);
 
         
 }
 
 public Post findPost(String postName){
    return this.fullArticleList.stream()
         .filter(post->postName.equals(post.Name))
         .findFirst()
         .orElse(null);
    
 
 }
    
}
