package com.jayway.rps;

public enum Move {
	rock, paper, scissor,lizard,spock;

	public boolean defeats(Move other) {
		switch (this) {
		case rock:
			return (other == scissor||other==lizard);
		case paper:
			return (other == rock||other==spock);
		case scissor:
			return (other == paper||other==lizard);
                case lizard:
                        return (other == spock || other==paper);
                        
                case spock:
                        return (other== rock || other== scissor);
		}
		return false;
	}
}
