/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvic2;

import java.util.Random;

/**
 *
 * @author jarda
 */
public class Tracker {
    Vehicle currentVehicle;
    int trackerId;
    int innerMemory;
    public Tracker(int id){
       this.trackerId= id;
        
    }
    public void attachTracker(Vehicle vehicle){
        this.currentVehicle=vehicle;
        this.innerMemory=vehicle.getMilage();
    }
    public int getTrackerMilage(){
        return currentVehicle.getMilage()-innerMemory;
        
    
    }

    public Vehicle getCurrentVehicle() {
        return currentVehicle;
    }
    public void resetTrackerMilage(){
        innerMemory=currentVehicle.getMilage();
    
    }
    public String toString(){
        return "Tracker_["+trackerId+"], attached to ["+currentVehicle.toString()+"]"; 
    
    }
    
}
