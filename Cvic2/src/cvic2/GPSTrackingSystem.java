/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvic2;

import java.util.List;

/**
 *
 * @author jarda
 */
public class GPSTrackingSystem {
    static int counter;
    List<Tracker> activeTrackers;

    public GPSTrackingSystem() {
       counter=0;
       
    }
    public void attachTrackingDevices(List<Vehicle> toTag){
        
        for(Vehicle car:toTag){
            Tracker newTracker=new Tracker(counter);
            newTracker.attachTracker(car);
            activeTrackers.add(newTracker);
        
        
        }
    
    
    }
    public void generateMonthlyReport(){
        System.out.println(" —– GPS Tracking system: Monthly report —– ");
        int totalTravaled=0;
        for (Tracker tracker:activeTrackers){
            System.out.println(tracker.toString());
            totalTravaled+=tracker.getTrackerMilage();
        }
        System.out.println("This month traveled distance: " +totalTravaled+ "Km. ");
    }
    
    
}
