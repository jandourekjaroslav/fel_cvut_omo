/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvic2;

/**
 *
 * @author jarda
 */
public class Vehicle {
    int milage;
    String manufacturer;
    String VINcode;
    public Vehicle(String madeBy,String vinC){
        this.VINcode=vinC;
        this.milage=0;
        this.manufacturer=madeBy;
        
    
    }
    public void drive(int milesToTravel){
        milage+=milesToTravel;
    }

    public int getMilage() {
        return milage;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getVINcode() {
        return VINcode;
    }
    
    public String toString(){
        return "["+manufacturer+"], "+"["+VINcode+"]";
        
    
    }
    
}
