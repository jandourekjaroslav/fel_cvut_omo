package cz.cvut.fel.omo.cv10;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;

public class TradeHistory {

    public List<Transaction> transactions;

    public TradeHistory(List<Transaction> transctions) {
        this.transactions = transctions;
    }

    public List<Transaction> findAllTransactionsIn2011AndSortByValueAsc(){
        List<Transaction> newList = new ArrayList<Transaction>();
       newList=transactions.stream().filter(transaction->transaction.getYear()==2011).sorted(Comparator.comparing(Transaction::getValue)).collect(Collectors.toList());
        return newList;
    }

    public List<String> getUniqueCitiesSortedAsc(){
        List<String> newList = new ArrayList<String>();
        //Implement body here
        newList=transactions.stream().map(s->s.getTrader().getCity()).distinct().sorted().collect(Collectors.toList());
        return newList;
    }

    /*
    * String shall start with "Traders:" and use space as separator. E.g.: "Traders: Bob George"
    *
     */
    public String getSingleStringFromUniqueTradersNamesSortByNameAsc(){
        String traderStr = "";
        traderStr=transactions.stream().map(s->s.getTrader()).map(s->s.getName()).distinct().sorted().reduce("Traders:",(a,b)->a+" "+b);
        return traderStr;
    }

    public boolean isSomeTraderFromCity(String cityName){
        boolean isSome = false;
        isSome=transactions.stream().map(s->s.getTrader()).anyMatch(trd->trd.getCity()==cityName);
        return isSome;
    }

    public Optional<Transaction> findSmallestTransactionUsingReduce(){
        Optional<Transaction> smallestTransaction = null;
            smallestTransaction=transactions.stream().distinct().reduce((t1,t2)->t1.getValue()<t2.getValue() ? t1:t2);
        return smallestTransaction;
    }

    public Map<String, List<Trader>> getTradersByTown(){
        Map<String, List<Trader>> tradersByTown = new HashMap<String, List<Trader>>();
        //Implement body here
        tradersByTown=transactions.stream().map(s->s.getTrader()).distinct().collect(Collectors.groupingBy(Trader::getCity));
        return tradersByTown;
    }

    public Map<String, Long> getTradersCountsByTown(){
        Map<String, Long> tradersByTown = new HashMap<String, Long>();
        tradersByTown=transactions.stream().map(s->s.getTrader()).distinct().collect(Collectors.groupingBy(Trader::getCity, Collectors.counting()));
        return tradersByTown;
    }

    public Map<Boolean, List<Transaction>> partitionTransactionsByTraderIsVegetarian(){
        Map<Boolean, List<Transaction>> transactionsBy = new HashMap<Boolean, List<Transaction>>();
        transactionsBy=transactions.stream().distinct().collect(Collectors.partitioningBy(l -> l.getTrader().isVegetarian()));
        
        return transactionsBy;
    }
}
