package cz.cvut.fel.omo.client;

import cz.cvut.fel.omo.Observer;
import cz.cvut.fel.omo.server.StockExchangeServer;

public class StockExchangeClient {

    private StockExchangeServer stockExchangeServer;
    private String name;
    private Observer bitcoin;
    private Observer litecoin;

    public StockExchangeClient(StockExchangeServer stockExchange, String name){
        this.stockExchangeServer = stockExchange;
        this.name = name;
    }

    /*
     * Method makes sure, that this client application is subscribed to the bitcoin channel.
     */
    public void subscribeToBitcoinChannel(){
        // IMPLEMENT ME
        if(bitcoin!=null){
            bitcoin=new BitcoinClient(this);
        stockExchangeServer.subscribeToBitcoinUpdates(bitcoin);}
    }

    /*
     * Method makes sure, that this client application is subscribed to the litecoin channel.
     */
    public void subscribeToLitecoinChannel(){
          if(litecoin!=null){
            litecoin=new LitecoinClient(this);
        stockExchangeServer.subscribeToLitecoinUpdates(litecoin);
        }
    }

    /*
     * Method makes sure, that this client application is subscribed to both litecoin and bitcoin channel.
     */
    public void subscribeToAllChannels(){
        subscribeToBitcoinChannel();
        subscribeToLitecoinChannel();
    }

    /*
     * Method unsubscribes application from bitcoin channel, hereafter no notifications about bitcoin price will be delivered.
     */
    public void unsubscribeFromBitcoinChannel(){
        stockExchangeServer.unsubscribeFromBitcoinChannel(bitcoin);
    }

    /*
     * Method unsubscribes application from litecoin channel, hereafter no notifications about litecoin price will be delivered.
     */
    public void unsubscribeFromLitecoinChannel(){
       stockExchangeServer.unsubscribeFromLitecoinChannel(bitcoin);
    }

    /*
     * Method unsubscribes application from both bitcoin and litecoin channels, hereafter no notifications about bitcoin nor litecoin price will be delivered.
     */
    public void unsubscribeFromAllChannels(){
        unsubscribeFromBitcoinChannel();
        unsubscribeFromLitecoinChannel();
    }

    public String getName(){
        return name;
    }

    public StockExchangeServer getServer(){
        return stockExchangeServer;
    }
}
