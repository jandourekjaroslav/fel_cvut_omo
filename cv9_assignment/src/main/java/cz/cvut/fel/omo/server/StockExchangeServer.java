
package cz.cvut.fel.omo.server;

        import cz.cvut.fel.omo.Observer;

        import javax.money.MonetaryAmount;
        import java.util.Random;

public class StockExchangeServer {

    int LITECOIN_RANGE = 15;
    int LITECOIN_COEFFICIENT = 5;
    int BITCOIN_RANGE = 20;
    int BITCOIN_COEFFICIENT = 8;
    CryptoCurrencyController bitcoin = new BitcoinController();
    CryptoCurrencyController litecoin= new LitecoinController();

    private static StockExchangeServer instance = null;

    public static StockExchangeServer getInstance() {
         if (instance == null){
            instance = new StockExchangeServer();
         }
        return instance;
    }
    
    
    /*
     * Method for subscribing Observer to Bitcoin channel.
     */
    public void subscribeToBitcoinUpdates(Observer observer) {
        bitcoin.attach(observer);
    }

    /*
    * Method for subscribing Observer to Litecoin channel.
    */
    public void subscribeToLitecoinUpdates(Observer observer) {
       litecoin.attach(observer);
    }

    /*
    * Method for unsubscribing Observer from Bitcoin channel.
    */
    public void unsubscribeFromBitcoinChannel(Observer observer){
        bitcoin.detach(observer);
    }

    /*
    * Method for unsubscribing Observer from Litecoin channel.
    */
    public void unsubscribeFromLitecoinChannel(Observer observer){
        litecoin.detach(observer);
    }

    /*
    * Method for computing new price for cryptocurrency.
    */
    public void computeMarketFluctuation() {
        computeBitcoinFluctuation();
        computeLitecoinFluctuation();
    }

    /*
    * Highly sophisticated method for computing new price for litecoin.
    */
    public void computeLitecoinFluctuation() {
        Random rand = new Random();
        int fluctuation = rand.nextInt(LITECOIN_RANGE) - LITECOIN_COEFFICIENT;
        litecoin.changePrice(fluctuation);
    }

    /*
    * Highly sophisticated method for computing new price for bitcoin.
    */
    public void computeBitcoinFluctuation() {
        Random rand = new Random();
        int fluctuation = rand.nextInt(BITCOIN_RANGE) - BITCOIN_COEFFICIENT;
        bitcoin.changePrice(fluctuation);
    }

    /*
     * Method for retrieving current price for litecoin.
     */
    public MonetaryAmount getLitecoinState(){
        return litecoin.getState();
    }

    /*
    * Method for retrieving current price for bitcoin.
    */
    public MonetaryAmount getBitcoinState(){
        return bitcoin.getState();
    }
}
