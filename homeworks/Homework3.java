


import java.util.ArrayList;


 class IteratorImpl implements CustomIterator{
    ArrayList<Integer> tree ;
    int index=-1;
    
     
     
    @Override
    public boolean hasNext() {
        return tree.size()-1>=index+1;
    }

    @Override
    public int next() {
        index++;
        return tree.get(index);
        
    }
    public ArrayList<Integer> serializeTreePreorder(Node root){
        tree =new ArrayList();
        tree.add(root.contents);
        IteratorImpl left,right;
        left=new IteratorImpl();
        right=new IteratorImpl();
        if (root.left!=null){
            
            tree.addAll(left.serializeTreePreorder(root.left));
        }
        if (root.right!=null){
            
            tree.addAll(right.serializeTreePreorder(root.right));
        
        }
        return this.tree;
    }
    public ArrayList<Integer> serializeTreeInorder(Node root){
        tree =new ArrayList();
        
        IteratorImpl left,right;
        left=new IteratorImpl();
        right=new IteratorImpl();
        if (root.left!=null){
            
            tree.addAll(left.serializeTreeInorder(root.left));
        }
        tree.add(root.contents);
        if (root.right!=null){
            
            tree.addAll(right.serializeTreeInorder(root.right));
        
        }
        return this.tree;
    
    
    } 
    public ArrayList<Integer> serializeTreePostorder(Node root){
         tree =new ArrayList();
        
        IteratorImpl left,right;
        left=new IteratorImpl();
        right=new IteratorImpl();
        if (root.left!=null){
            
            tree.addAll(left.serializeTreePostorder(root.left));
        }
        
        if (root.right!=null){
            
            tree.addAll(right.serializeTreePostorder(root.right));
        
        }
        tree.add(root.contents);
        return this.tree;
    
    
    } 
 }
class Node {
    final int contents;
    final Node left, right;
    Node parent;
 
    Node(int contents, Node left, Node right) {
        this.contents = contents;
        this.left = left;
        if (left != null) left.parent = this;
        this.right = right;
        if (right != null) right.parent = this;
    }
 
    public CustomIterator preorderIterator() {
        IteratorImpl iterator= new IteratorImpl();
        iterator.serializeTreePreorder(this);
        return iterator;
    }
    public CustomIterator inorderIterator() {   
        IteratorImpl iterator= new IteratorImpl();
        iterator.serializeTreeInorder(this);
        return iterator;
    }
    public CustomIterator postorderIterator() {
        IteratorImpl iterator= new IteratorImpl();
        iterator.serializeTreePostorder(this);
        return iterator;
    }

}
