/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo;

import java.util.NoSuchElementException;

/**
 *
 * @author jarda
 */
public class ReverseArrayIterator implements Iterator{
    int[] array;
    int index;
    
    public ReverseArrayIterator(int[] array){
        this.array=array;
        index=array.length-1;
                
    
    }
    @Override
    public int currentItem() {
        if(array.length>0){return array[index];}
        throw new NoSuchElementException();
        
    }

    @Override
    public int next() {
       if(index>=1){
        index--;
        return array[index];
       } 
     throw new NoSuchElementException();

    
    }

    @Override
    public boolean isDone() {
        return index==0;
    }

    @Override
    public int first() {
        index=array.length-1;
        return array[index];
    
    }    
}
