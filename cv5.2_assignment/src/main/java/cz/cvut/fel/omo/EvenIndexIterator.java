/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo;

import java.util.NoSuchElementException;

/**
 *
 * @author jarda
 */
public class EvenIndexIterator implements Iterator{
    int[] array;
    int index;

    public EvenIndexIterator(int[] array) {
        this.array=array;
        this.index=0;
    }
    
    
    
    @Override
    public int currentItem() {
        if(array.length>0){return array[index];}
        throw new NoSuchElementException();
    }

    @Override
    public int next() {
       if(array.length-1>=index+2){
        index+=2;
        return array[index];
       } 
     throw new NoSuchElementException();

    }

    @Override
    public boolean isDone() {
         if(array.length-1==index || array.length-1 < index+2){
        
        return true;
       } 
         return false;
    }

    @Override
    public int first() {
        index=0;
        return array[index];
    }
    
}
