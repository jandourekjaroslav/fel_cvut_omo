/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo;

import java.util.EmptyStackException;
import java.util.NoSuchElementException;

/**
 *
 * @author jarda
 */
public class ExtendedStackImpl implements ExtendedStack{
    private  int STACK_SIZE = 1000;
    Stack stack= new StackImpl();
    
    @Override
    public void push(int toInsert) {
        stack.push(toInsert);
    }

    
    @Override
    public void push(int[] toInsert) {
        for (int toPush:toInsert){
            this.push(toPush);
        
        }
    }

    @Override
    public int top() {
        int poped;
        poped=stack.pop();
        this.push(poped);
        return poped;
    }

    @Override
    public int pop() {
        return stack.pop();
               
    }

    @Override
    public int popFirstNegativeElement() {
        Stack helpStack=new StackImpl();
        if (stack.getSize()<=0){
            throw new EmptyStackException();
        }
       for (int i=0; i<stack.getSize();i++){
            if(this.top()<0){
                int returnNumb=this.pop();
                
                while(helpStack.getSize()>0){
                    this.push(helpStack.pop());
                
                }
                
                return returnNumb;
                        
            
            }
            else{
               helpStack.push(this.pop());
            }
        
        }
       
         throw new NoSuchElementException();
    }

    @Override
    public boolean isEmpty() {
        return stack.getSize()==0;
    }

    @Override
    public int getSize() {
        return stack.getSize();
        
    }
    
}
