package cz.cvut.fel.omo.cv6.strategy;

import cz.cvut.fel.omo.cv6.Street;
import cz.cvut.fel.omo.cv6.TrafficLight;
import cz.cvut.fel.omo.cv6.strategy.Strategy;

public class EveningStrategy extends Strategy{

    int time = 0;

    public EveningStrategy(Street street) {
        super(street);
    }

    @Override
    public void controlTraffic() {

        int counter = 0;

        for (int i =street.getLights().size()-1; i>=0; i--) {
            TrafficLight light=street.getLights().get(i);
            if (time - counter * lightDistance == 0) {
                light.startGoSequence();
            } else {
                light.timeLapseTick();
            }
            counter++;
        }
        time++;

    }

}
