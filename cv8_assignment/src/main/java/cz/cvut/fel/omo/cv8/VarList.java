package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;

public class VarList implements ListExpression{
    protected final String name;

    public VarList(String name) {
        this.name = name;
    }

    @Override
    public ImmutableList<Integer> evaluate(Context c) {
       return c.get(name);
    }

    @Override
    public void accept(ListExpressionVisitor v) {
       
    }

}
