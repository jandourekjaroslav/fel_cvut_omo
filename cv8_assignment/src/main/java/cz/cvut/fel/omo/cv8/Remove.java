package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;

public class Remove implements ListExpression{
    protected final ListExpression sub;
    protected int element;

    public Remove(ListExpression sub, int element) {
        this.sub=sub;
        
        this.element = element;
    }

    @Override
    public ImmutableList<Integer> evaluate(Context c) {
         List<Integer> list = new ArrayList<>(sub.evaluate(c));
         while(list.contains(element)){list.remove(element);};
         
         return ImmutableList.copyOf(list);
    }

    @Override
    public void accept(ListExpressionVisitor v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
