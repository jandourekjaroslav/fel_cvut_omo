package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;

public class Unique implements ListExpression{
    protected final ListExpression sub;

    public Unique(ListExpression sub) {
        this.sub = sub;
    }

    @Override
    public ImmutableList<Integer> evaluate(Context c) {
        List<Integer> list = new ArrayList<>(sub.evaluate(c)),listOfFound=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            if(!listOfFound.contains(list.get(i))){
                listOfFound.add(list.get(i));
            
            }
        
        }
        return ImmutableList.copyOf(listOfFound);
    }

    @Override
    public void accept(ListExpressionVisitor v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}