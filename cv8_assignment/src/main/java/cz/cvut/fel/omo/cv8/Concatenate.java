package cz.cvut.fel.omo.cv8;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;

public class Concatenate implements ListExpression{
    protected final ListExpression left;
    protected final ListExpression right;

    public Concatenate(ListExpression left, ListExpression right) {
        this.left = left;
        this.right = right;
    }



  

   
    public ImmutableList<Integer> evaluate(Context c) {
       List<Integer> list = new ArrayList<>(left.evaluate(c));
       List<Integer> listright = new ArrayList<>(left.evaluate(c));
       list.addAll(listright);
       return ImmutableList.copyOf(list);
       
    }

    @Override
    public void accept(ListExpressionVisitor v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
  
}
