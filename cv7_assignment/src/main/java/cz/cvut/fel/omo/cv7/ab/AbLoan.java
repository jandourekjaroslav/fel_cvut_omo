/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.cv7.ab;

import cz.cvut.fel.omo.cv7.Loan;
import javax.money.MonetaryAmount;
import org.javamoney.moneta.Money;

/**
 *
 * @author jarda
 */
public class AbLoan implements Loan{
     MonetaryAmount balance;
    double interestRate;
    int repaymentPeriod;

    public AbLoan(MonetaryAmount amount, int months, double recommendedInterestRate){
        balance = amount;
        interestRate = recommendedInterestRate;
        repaymentPeriod = months;
    }

    @Override
    public MonetaryAmount getBalance() {
        return balance;
    }

    @Override
    public double getInterestRate() {
        return interestRate;
    }

    @Override
    public MonetaryAmount getMonthlyPayment() {
        return balance=balance.divide(repaymentPeriod).add(balance.multiply(interestRate/12)).add(Money.of(10, "EUR"));
    }

    public String toString(){
        return String.format("Loan Overview - Balance: %s, InterestRate: %f, MonthlyPayment: %s", getBalance(), getInterestRate(), getMonthlyPayment());
    }

}
