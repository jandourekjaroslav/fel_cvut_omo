/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.cv7.ab;
import cz.cvut.fel.omo.cv7.Account;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;

/**
 *
 * @author jarda
 */
public class AbAccount implements Account{
    MonetaryAmount fee=Money.of(0,"EUR");
    
     MonetaryAmount balance = Money.of(0,"EUR");

    @Override
    public MonetaryAmount getBalance() {
        return balance;
    }

    @Override
    public MonetaryAmount getWithdrawLimit() {
        return Money.of(2000,"EUR");
    }

    @Override
    public  MonetaryAmount getMonthlyFee() {
        return fee;
    }

    @Override
    public void withdraw(MonetaryAmount amount) {
        balance=balance.subtract(amount);
        fee=fee.add(Money.of(0.5, "EUR"));
    }

    @Override
    public void deposit(MonetaryAmount amount) {
        balance=balance.add(amount);
        fee=fee.subtract(Money.of(0.25, "EUR"));

    }

    @Override
    public String toString(){
        return String.format("Ab Account - balance: %s, fee: %s", getBalance(), getMonthlyFee());
    }
}
