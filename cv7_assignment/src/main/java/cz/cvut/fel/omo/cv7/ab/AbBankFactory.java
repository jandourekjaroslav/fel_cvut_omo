/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.omo.cv7.ab;


import cz.cvut.fel.omo.cv7.AbstractBankFactory;
import cz.cvut.fel.omo.cv7.Account;
import cz.cvut.fel.omo.cv7.BankOffice;
import cz.cvut.fel.omo.cv7.Loan;
import javax.inject.Named;
import javax.money.MonetaryAmount;
import org.springframework.context.annotation.Primary;


/**
 *
 * @author jarda
 */
 @Named("Ab") 
 @Primary
public class AbBankFactory extends AbstractBankFactory {
    private static AbBankFactory instance=null;
    
    

    public static AbBankFactory getInstance() {
        if(instance==null){
            instance=new AbBankFactory();
        }
        
        return instance;
    }
    
    
    @Override
    public BankOffice createBankOffice() {
        return new AbBankOffice();
    }

    @Override
    public Account createAccount() {
        return new AbAccount();
    }

    @Override
    public Loan createLoan(MonetaryAmount amount, int months, double recommendedInterestRate) {
        return new AbLoan(amount, months, recommendedInterestRate);
    }
}
