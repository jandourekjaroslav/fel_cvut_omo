/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvic1;

import java.time.Year;

/**
 *
 * @author jarda
 */
public class AutoFactory {
       public  int carsCreated=0;
       public  Auto makePersonal(){
       carsCreated++;
           System.out.println("Made a personal car");
       return new Auto(Year.now(),"", 17, 4);
       }
       public  Auto makeTruck(){
       carsCreated++;
           System.out.println("Made a Truck");
       return new Auto(Year.now(),"", 50, 6);
       
       
       }

    public int getCarsCreated() {
        return carsCreated;
    }
       
}
